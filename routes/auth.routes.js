const {Router} = require('express')
const router = Router()

const test = require('./auth.routes/test')
router.post('/test', test)

router.post('/login', test)
router.post('/registration', test)
router.post('/logout', test)
router.post('/recover', test)
router.get('/confirm/*/', test)

module.exports = router
