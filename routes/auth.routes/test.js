const test = async (req, res) => {
    try {
        // get needle fields from request
        const {login, password, test} = req.body

        // can throw exceptions on errors
        if (test !== undefined) throw new Error('invalid arg available!')

        // on success:
        res.status(200).json({
            // some answers...
            login: login,
            password: password,
            path: req.baseUrl + req.url
        })
    } catch (e) {
        // on server errors:
        res.status(500).json({
            e: e.message
        })
    }
}

module.exports = test
