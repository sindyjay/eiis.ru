// load core modules
const config = require('./app.config.json');
const express = require('express')
const app = express()

app.use(express.json())
app.use('/api/auth', require('./routes/auth.routes'))

app.all('*', async (req, res) => {
    res.status(404).json({
        message: "unknown page! its a custom 404, Mthf-r",
        uri: req.originalUrl,
        method: req.method
    })
})

// start application:
app.listen(config.PORT, () => console.log(`Server has been started on port ${config.PORT}...`));
